# docker-clair-scanner

https://github.com/arminc/clair-scanner

Goals:

* Completely containerized option for Clair container vulnerability scans
* Maintenance self-sufficiency, with automatic weekly builds
* Helpful documentation


# quick start

```
IMAGE=your-cool-image
TAG=latest

# get a random value to use in enabling concurrency
RAND=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)

# start vulnerability db
docker run --rm -d --name local-clair-db-$RAND arminc/clair-db:latest

# start clair service, vulnerability index
docker run --rm -d --name local-clair$RAND \
                               --link local-clair-db$RAND:postgres \
                               -p 6060:6060 \
                                arminc/clair-local-scan:latest

# start clair scanner, read-only access to docker socket
docker run --rm --name=scanner-$RAND \
                           --net=scanning-$RAND \
                           --link=clair-$RAND:clair \
                           -v '/var/run/docker.sock:/var/run/docker.sock:ro' \
                           registry.gitlab.com/adrift/docker-clair-scanner \
                           --clair="http://clair-$RAND:6060" \
                           --ip="scanner-$RAND" \
                           -r report.json \
                           $IMAGE:$TAG

echo "\n\nImage vulnerability scan complete. Important artifacts: report.json\n\n"
```
## further reading

[CoreOS Clair](https://github.com/coreos/clair)

[arminc](https://github.com/arminc)'s Clair utils

* https://github.com/arminc/clair-local-scan
* https://github.com/arminc/clair-scanner

## credits
Big thanks to [ObjectifLibre](https://github.com/ObjectifLibre/docker-clair-scanner) for thinking of making this image first, and releasing it with the MIT license.

Creating this was much easier when reducing that initial information ramp-up.
