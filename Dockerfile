FROM alpine

RUN apk update && apk add go git curl make musl-dev && rm -rf /var/cache/apk/*

ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV GOBIN /usr/bin

RUN git clone https://github.com/arminc/clair-scanner.git $GOPATH/src/clair

WORKDIR $GOPATH/src/clair

RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh && \
 make ensure && \
 make build

FROM alpine

ENV GOROOT /usr/lib/go
ENV GOPATH /go
ENV GOBIN /usr/bin

EXPOSE 9279

COPY --from=0 $GOPATH/src/clair/clair /usr/local/bin

ENTRYPOINT ["clair"]

CMD []